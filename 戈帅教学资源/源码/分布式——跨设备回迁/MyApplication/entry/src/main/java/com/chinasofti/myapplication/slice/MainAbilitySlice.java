package com.chinasofti.myapplication.slice;

import com.chinasofti.myapplication.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.IAbilityConnection;
import ohos.aafwk.ability.IAbilityContinuation;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.agp.components.Button;
import ohos.agp.components.TextField;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

/**
 * 1、config.json中声明权限
 * 2、Ability、slice实现接口 IAbilityContinuation
 * 3、实现的方法返回值修改为true
 * 4、保存数据：在onSaveData使用(key-value)
 * 5、取数据：onRestoreData,类型转换时需要类型判断 instanceof
 * 6、更新UI,在onActive
 */

public class MainAbilitySlice extends AbilitySlice implements IAbilityContinuation {
    TextField tf;
    String content=null;

    MyEventHandler myEventHandler;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main2);

        tf = (TextField)findComponentById(ResourceTable.Id_tf);

        myEventHandler = new MyEventHandler(EventRunner.getMainEventRunner());
        findComponentById(ResourceTable.Id_btn).setClickedListener(list->{
            try {
                continueAbilityReversibly();
            }catch (Exception e){

            }
        });
        findComponentById(ResourceTable.Id_btn2).setClickedListener(list->{
            try {
                reverseContinueAbility();
            }catch (Exception e){

            }
        });
        tf.setText(content);
    }

    @Override
    public void onActive() {
        super.onActive();
        tf.setText(content);
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public boolean onStartContinuation() {
        return true;
    }

    @Override
    public boolean onSaveData(IntentParams intentParams) {
        intentParams.setParam("tf",tf.getText());
        return true;
    }

    @Override
    public boolean onRestoreData(IntentParams intentParams) {

        try{
            content = (String)intentParams.getParam("tf");
        }catch (Exception e){

        }
        if(myEventHandler !=null)
            myEventHandler.sendEvent(1);
        return true;
    }

    @Override
    public void onCompleteContinuation(int i) {

    }

    @Override
    public void onRemoteTerminated() {
    }
    class MyEventHandler extends EventHandler{

        public MyEventHandler(EventRunner runner) throws IllegalArgumentException {
            super(runner);
        }

        @Override
        protected  void processEvent(InnerEvent event){
            super.processEvent(event);
            if (tf!=null)
                tf.setText(content);
        }
    }
}
