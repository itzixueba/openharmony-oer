package com.chinasofti.myapplication;

import com.chinasofti.myapplication.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.IAbilityContinuation;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;

import java.util.ArrayList;
import java.util.List;

import static ohos.data.search.schema.PhotoItem.TAG;

public class MainAbility extends Ability implements IAbilityContinuation {
    @Override
    public void onStart(Intent intent) {
        String[] permissions = {
                "ohos.permission.READ_USER_STORAGE",
                "ohos.permission.WRITE_USER_STORAGE",
                "ohos.permission.DISTRIBUTED_DATASYNC"
        };
        requestPermissionsFromUser(permissions, 0);

        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());

    }

    @Override
    public boolean onStartContinuation() {
        return true;
    }

    @Override
    public boolean onSaveData(IntentParams intentParams) {
        return true;
    }

    @Override
    public boolean onRestoreData(IntentParams intentParams) {
        return true;
    }

    @Override
    public void onCompleteContinuation(int i) {
    }

    @Override
    public void onRemoteTerminated() {

    }
}
