package com.chinasofti.distribute6.slice;

import com.chinasofti.distribute6.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.distributedschedule.interwork.DeviceInfo;
import ohos.distributedschedule.interwork.DeviceManager;

import java.util.List;

/**
 * 1、添加Button，添加点击事件
 * 2、获取设备列表、并获取第一个设备设备ID
 * 3、Button的点击事件中，调用startAbility，把设备ID
 * 4、添加权限
 */
public class MainAbilitySlice extends AbilitySlice {
    Button button;
    int count = 0;
    @Override
    public void onStart(Intent intent) {
        // 开发者显示声明需要使用的权限
        requestPermissionsFromUser(new String[]{"ohos.permission.DISTRIBUTED_DATASYNC"}, 0);
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        //1、添加Button，添加点击事件
        count = intent.getIntParam("count",0);
        button = (Button) findComponentById(ResourceTable.Id_btn);
        button.setText(button.getText()+count);
        button.setClickedListener(lis->{
            //2、获取设备列表、并获取第一个设备设备ID
            List<DeviceInfo> onlineDevices =  DeviceManager.getDeviceList(DeviceInfo.FLAG_GET_ONLINE_DEVICE);
            if(onlineDevices.size()==0)
                return ;
            String deviceId = onlineDevices.get(0).getDeviceId();

            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId(deviceId)
                    .withBundleName("com.chinasofti.distribute6")
                    .withAbilityName("com.chinasofti.distribute6.MainAbility")
                    .withFlags(Intent.FLAG_ABILITYSLICE_MULTI_DEVICE)
                    .build();

            intent.setOperation(operation);
            count++;
            intent.setParam("count",count);
            //3、Button的点击事件中，调用startAbility，把设备ID
            startAbility(intent);
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
