package com.chinasofti.myapplication2.slice;

import com.chinasofti.myapplication2.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.IAbilityContinuation;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.agp.components.TextField;

public class MainAbilitySlice extends AbilitySlice implements IAbilityContinuation {
    TextField tf;
    String content = null;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        tf = (TextField) findComponentById(ResourceTable.Id_tf);
        findComponentById(ResourceTable.Id_btn).setClickedListener(list->{
            continueAbility();
            terminateAbility();
        });
        tf.setText(content);
    }

    @Override
    public void onActive() {
        super.onActive();
        tf.setText(content);
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public boolean onStartContinuation() {
        return true;
    }

    @Override
    public boolean onSaveData(IntentParams intentParams) {
        intentParams.setParam("tf",tf.getText());
        return true;
    }

    @Override
    public boolean onRestoreData(IntentParams intentParams) {
        try {
            content = (String) intentParams.getParam("tf");
        }catch (Exception e){

        }

        return true;
    }

    @Override
    public void onCompleteContinuation(int i) {

    }

    @Override
    public void onRemoteTerminated() {

    }
}
