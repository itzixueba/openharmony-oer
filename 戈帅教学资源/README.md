# 戈帅OpenHarmony教学资源仓

本资源仓提供OpenHarmony相关教学资源，主要包括：教学代码、教学视频地址、PPT等
## 资源目录

01、HarmonyOS分布式任务调试
视频地址：https://www.bilibili.com/video/BV1CV411473L/

02、鸿蒙分布式任务调试——数据传递
视频地址：https://www.bilibili.com/video/BV1Bb4y1d7V1/

03、分布式——跨设备迁移
视频地址：https://www.bilibili.com/video/BV1u64y197eh/

04、分布式——跨设备回迁
视频地址：https://www.bilibili.com/video/BV1Hh411h7qD

05、鸿蒙应用c++模板开发详解
视频地址：https://www.bilibili.com/video/BV1Qq4y1Z7W3

06、HarmonyOS服务卡片快速开
视频地址：https://www.bilibili.com/video/BV1Vq4y1L76K/

07、HarmonyOS时钟FA卡片开发样例介绍
视频地址：https://www.bilibili.com/video/BV1hV411s7YH/

08、ComponentCodelab——HarmonyOS 分布式亲子教育（实操）
视频地址：https://www.bilibili.com/video/BV1oQ4y1X7Vz/

09、HarmonyOS 分布式亲子教育——操作演示
视频地址：https://www.bilibili.com/video/BV1mU4y1L7Wh/

10、ComponentCodelab——体验TabList和Tab组件（实操）
视频地址：https://www.bilibili.com/video/BV1Sf4y1a7dZ/

11、ComponentCodelab——下载Codelab起步应用（实操）
视频地址：https://www.bilibili.com/video/BV1Sh411m7XQ/

12、ComponentCodelab——Tablist的使用方法
视频地址：https://www.bilibili.com/video/BV1U54y157mq/

13、HarmonyOS APP《拼夕夕》演示
视频地址：https://www.bilibili.com/video/BV1GM4y157eC/

14、2021未来杯高校AI挑战赛 > HarmonyOS技术应用创新大赛——《救援小车》
视频地址：https://www.bilibili.com/video/BV1Xb4y127Jc

15、调色器演示
视频地址：https://www.bilibili.com/video/BV1Qb4y1y76E

16、分布式云笔记演示
视频地址：https://www.bilibili.com/video/BV1AL411x7Yg

17、分布式文件案例
视频地址：https://www.bilibili.com/video/BV1Eh411n7sL

18、分布式文件文件浏览器
视频地址：https://www.bilibili.com/video/BV1t44y1i7W7

** 当资源积累到一定程度时希望可以出一本有关鸿蒙相关的书籍  **